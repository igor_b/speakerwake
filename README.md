# Speakerwake

A simple script and systemd timer+service to play short high pitched (unhearable) sound every minute to prevent speakers from going to sleep while the monitor is on.

A sound which is played is 3 second long 16kHz sine tone.

## Configuration

`speakerwake.sh` uses `play` for playing sound and assumes that the default screen in :1. Change if needed.
Systemd service script `speakerwake.service` needs to be updated with the correct path to the `speakerwake.sh` script

## Instalation

copy the `speakerwake.service` and `speakerwake.timer` files to `~/.config/systemd/user` directory and run

```
systemctl --user enable speakerwake.timer   # start at boot
systemctl --user start speakerwake.timer    # start the timer
```

To stop it use "stop" instead of "start", to disable it on boot use "disable". `systemctl --user list-timers` will show the current status.


## TODO

Maybe add a makefile for installing/uninstalling...
