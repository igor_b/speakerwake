#!/bin/bash

DISPLAY_INDEX=1
PLAY_CMD=play
SOUNDFILE=$(dirname "$(readlink -f "$0")")/wakesound.wav

# exit if screen is off
export DISPLAY=:${DISPLAY_INDEX}
xset q | grep "Monitor is Off" &> /dev/null
if [ $? -eq 0 ]; then
  exit 0
fi

${PLAY_CMD} $SOUNDFILE &> /dev/null

